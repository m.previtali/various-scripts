Coverted the old Hertzian model with rolling resistance to PFC7. The debug example seems to work (4 spheres with linear and hertizan model, 2 with rolling resistance and 2 without, are pushed on a plane)

The source code and project setup are in
"PluginFiles\contactmodel\example"

The dlls and debug projects are located in:
"PluginFiles\contactmodel\example\x64\Debug" and "PluginFiles\contactmodel\example\x64\Release"

I don't think the rest of the folder is needed but I kept it just in case it contains dependencies for the code



