#pragma once
/** \file base.h
  * \brief One stop include for all objects defined as part of base interface.
  * \addtogroup Base Base interface specification 
  * @{
  */

#include "basedef.h"
#include "basestring.h"
#include "to.h"
#include "extent3.h"
#include "avect.h"
#include "basememory.h"
#include "variant.h"
#include "symtensor.h"
#include "orientation.h"
#include "quat.h"
#include "caxes.h"

/// @}
// EoF
