#pragma once
// idynzone.h

/**
  * \file
  * \brief Interface to access dynamic zone data
  */

namespace zone {
    /// \brief Interface to access dynamic zone data
    /// \ingroup Zone
    class IDynZone {
    public:
    };
} // namespace zone
// EoF
