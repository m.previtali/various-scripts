#pragma once

namespace body {

    class IModuleBody {
    public:
        virtual void                     test()=0;
    };
} // namespace body
// EoF
