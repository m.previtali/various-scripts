Examples of P-Y curves and what they are used for:
https://www.pilegroups.com/p-y-curves

This python script just generates a beam with an X number of springs connected along its length. 
All the springs are simply linear right now. To do: add a numpy function to load txt files from the folder.
![](https://i.imgur.com/MnkIIv5.png)
![](https://i.imgur.com/287e4gg.png)
